"""
написать функцию, которая принимает N целых чисел и возвращает список квадратов этих чисел. Бонусом будет сделать keyword аргумент для выбора степени, в которую будут возводиться числа
    Критерии оценки:
        функция для возведения N чисел в степень - 1 балл
        функция использует map
        используется не лямбда, а функция из встроенного модуля operator
        используется keyword для выбора степени, присутствует значение по умолчанию

написать функцию, которая на вход принимает список из целых чисел, и возвращает только чётные/нечётные/простые числа (выбор производится передачей дополнительного аргумента)
    Критерии оценки:
        функция для фильтрации чисел - 1 балл
        функция использует filter
        для проверки типа фильтрации используются константы (переменные в UPPER_SNAKE_CASE)
        для проверки простое ли число создана отдельная функция
        
создать декоратор для замера времени выполнения функции
    Критерии оценки:
        создание декоратора для замера времени выполнения функции - 1 балл
        внутри обёртки используется wraps из functools
        декоратор trace - 1 балл
        написана функция для вычисления чисел Фибоначчи
        функция для вычисления чисел Фибоначчи использует рекурсию
        обёртка работает и показывает погружения
        для всех написанных функций есть пример их работы - 1 балл
        данные выводятся красиво и понятно - 1 балл
"""
import time
import random
from functools import wraps

# First task
def exponentiation(*args,exponent=2):
    return list(map(pow,args,[exponent for _ in args]))

# Second task
def my_filter(l,filter_type):
    """
    l : list of ints
    filter_type : even | not_even | simple_number
    """
    def even(l):
        return list(filter(lambda i:i%2==0,l))
    def not_even(l):
        return list(filter(lambda i:i%2!=0,l))
    def simple_number(l):
        simple_nums=set()
        if max(l)<2:
            return [] 
        else:
            simple_nums.add(2)
        for i in range(3,max(l)+1,2):
            is_simple=True
            for x in simple_nums:
                if i%x==0:
                    is_simple=False
                    break
            if is_simple:
                simple_nums.add(i)
        # return [i for i in l if i in simple_nums]
        return list(filter(lambda i:i in simple_nums,l))
    types={"even":even,"not_even":not_even,"simple_number":simple_number}
    return types[filter_type](l)

#Third task
def execution_time(func,*args):
    start=time.time()
    res = func(*args)
    end=time.time()
    print("Execution time: ",end-start)
    return res

def execution_time_decorator(func):
    @wraps(func)
    def wrapper(*args):
        return execution_time(func,*args)
    return wrapper


@execution_time_decorator
def task():
    rnd=random.random()
    time.sleep(rnd)
    return rnd

@execution_time_decorator
def fib(n):
    fib_nums=[0,1]
    if n<0:
        return
    elif n<=1:
        return fib_nums[n]
    for i in range(2,n+1):
        fib_nums.append(fib_nums[i-1]+fib_nums[i-2])
    return fib_nums[-1]


def rec_fib(n): # recursive fibonachi without hash
    if n==0:
        return 0
    if n==1:
        return 1
    return rec_fib(n-1)+rec_fib(n-2)



#Examples
print("Example for first task: ")
args=[i for i in range(17)]
print(exponentiation(*args,exponent=3))
print()
print("Example for second task: ")
l,t=[i for i in range(12)],"even"
print("Test data: ",l,t)
print(my_filter(l,t))
t="not_even"
print("Test data: ",l,t)
print(my_filter(l,t))
t="simple_number"
print("Test data: ",l,t)
print(my_filter(l,t))
print()
print("Example for third task: ")
print("Random float from 0 to 1, programm sleeps:",task())
print("Fib function from 10:",fib(10))
print("Fib recursive function from 10:",rec_fib(10))
